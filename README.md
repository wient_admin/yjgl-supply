<!--
 * @Description: 
 * @Version: 2.0
 * @Autor: Wient Mo
 * @Date: 2022-10-09 15:35:19
 * @LastEditors: Wient Mo
 * @LastEditTime: 2022-10-15 17:13:07
-->

## yjgl-php-sdk


## 安装

* 通过composer，这是推荐的方式，可以使用composer.json 声明依赖，或者运行下面的命令。
```bash
$ composer require yjgl-supply/php-sdk
```
* 直接下载安装，SDK 没有依赖其他第三方库，但需要参照 composer的autoloader，增加一个自己的autoloader程序。

## 运行环境

    php: >=7.0

## 使用方法

```php    
    use Yjgl\Api\SupplyClient;
    //appkey、appSecret 联系管理员获取
    $appKey = "your appkey"; 
    $appSecret = "your appSecret";
    
    try {
    	$supplyClient = new SupplyClient($appKey,$appSecret);
    } catch (OssException $e) {
    	printf(__FUNCTION__ . "creating supplyClient instance: FAILED\n");
    	printf($e->getMessage() . "\n");
    	return null;
    }
    
    //获取测试列表
    $param = ['page'=>1, 'limit'=>20, 'source'=>2];//请求参数
    $method = 'get';//请求方法
    $action = 'v1/index/index';//请求资源名
    $response = $supplyClient->getApiResponse($method,$action,$param);
```    





